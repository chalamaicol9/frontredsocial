import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'

import './assets/fonts/fontawesome-free-6.1.2-web/css/all.css';
import './assets/css/normalize.css';
import './assets/css/styles.css';
import './assets/css/responsive.css';
import './assets/css/iziToast.min.css';
import './assets/js/iziToast.min.js';

ReactDOM.createRoot(document.getElementById('root')).render(
 
    <App />

)

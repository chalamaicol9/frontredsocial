import React from "react";
import { useState, useEffect, createContext } from "react";
import { GLOBAL } from "../helpers/GLOBAL";


const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useState({});
  const [counters, setCounters] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    authUser();
  }, []);

  const authUser = async () => {
    const token = localStorage.getItem("token");
    const user = localStorage.getItem("user");
    if (!token || !user) {
      setLoading(false);
      return false;
    }
    const userObj = JSON.parse(user);
    const userId = userObj.id;
    const request = await fetch(GLOBAL.url + "user/profile/" + userId, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Authorization": token
      }
    });

    const data = await request.json();
    const requestCount = await fetch(GLOBAL.url + "user/counters/" + userId, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Authorization": token
      }
    });
    const dataCount = await requestCount.json();

    setAuth(data.user);
    setCounters(dataCount);
    setLoading(false);


  }

  return (
    <AuthContext.Provider value={{
      auth,
      setAuth,
      counters,
      loading,
      setCounters
    }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
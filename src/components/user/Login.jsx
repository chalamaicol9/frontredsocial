import React from "react";
import { useForm } from "../../hooks/useForm";
import { GLOBAL } from "../../helpers/GLOBAL";
import { useState } from "react";
import  useAuth  from "../../hooks/useAuth";


export const Login = () => {

  const { form, changed } = useForm({});
  const [saved, setSaved] = useState("not_sended");

  const {setAuth} = useAuth();

  const loginUser = async (e) => {
    e.preventDefault();
    let userTologin = form;
    console.log(userTologin);
    const response = await fetch(GLOBAL.url + 'user/login', {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(userTologin)
    });
    const data = await response.json();

    if (data.status === "success") {
      setSaved("login");
      localStorage.setItem("token", data.token);
      localStorage.setItem("user", JSON.stringify(data.user));
      setAuth(data.user);
      setTimeout(() => {
        window.location.reload();
        //window.location.href = "/social/feed";
      }, 2000);

    } else {
      setSaved("error");
    }
  }

    return (
      <>
        <header className="content__header content__heder--public">
          <h1 className="content__title">Login</h1>

        </header>

        <div className="content__posts">
          {saved == "login" ? <strong className="alert alert-success">Usuario identificado correctamente!!</strong>
            : ''}
          {saved == "error" ? <strong className="alert alert-danger">Error al identificarse!!</strong>
            : ''}
          <form className="form-login" onSubmit={loginUser}>
            <div className="form-group">
              <label htmlFor="email">Correo</label>
              <input type="email" id="email" name="email" required onChange={changed} />
            </div>
            <div className="form-group">
              <label htmlFor="password">Contraseña</label>
              <input type="password" id="password" name="password" required onChange={changed} />
            </div>

            <input type="submit" value="Iniciar sesión" className="btn btn-success" />


          </form>

        </div>
      </>
    )
  };
import React from "react";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import  useAuth from "../../hooks/useAuth";
export const Logout = () => {
    const Navigate = useNavigate();
    const {setAuth,setCounters} = useAuth();

    useEffect(() => {
        localStorage.clear();
        localStorage.removeItem("user");
        setAuth({});
        setCounters({});
        Navigate("/login");

    });
    return (
        <>
            <h1>Cerrando sesión...</h1>
        </>
    )
}
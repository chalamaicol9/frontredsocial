import React from "react";
import { useForm } from "../../hooks/useForm";
import { GLOBAL } from "../../helpers/GLOBAL";
import { useState } from "react";

export const Register = () => {

  const { form, changed } = useForm({});
  const [ saved, setSaved ] = useState("not_sended");

  const saveUser = async (e) => {
    e.preventDefault();
    let newUser = form;
    // guardar en el backend
    const request = await fetch(GLOBAL.url + 'user/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newUser)
    });
    const data = await request.json();

    if (data.status === 'success') {
      setSaved('sended');
    } else {
      setSaved('error');
    }

  }
  return (
    <>
      <header className="content__header content__heder--public">
        <h1 className="content__title">Registro</h1>

      </header>

      <div className="content__posts">
      { saved == "sended" ? <strong className="alert alert-success">Usuario registrado correctamente !!</strong>
        : ''}
      { saved == "error" ? <strong className="alert alert-danger">Error al registrar usuario !!</strong>
        : ''}
        <form className="register-form" onSubmit={saveUser}>
          <div className="form-group">
            <label htmlFor="name">Nombre</label>
            <input type="text" id="name" name="name" onChange={changed} required />
          </div>
          <div className="form-group">
            <label htmlFor="surname">Apellidos</label>
            <input type="text" id="name" name="surname" onChange={changed} required />
          </div>
          <div className="form-group">
            <label htmlFor="nick">Nick</label>
            <input type="text" id="name" name="nick" onChange={changed} required />
          </div>
          <div className="form-group">
            <label htmlFor="email">Correo</label>
            <input type="email" id="email" name="email" onChange={changed} required />
          </div>
          <div className="form-group">
            <label htmlFor="password">Contraseña</label>
            <input type="password" id="password" name="password" onChange={changed} required />
          </div>
          <div className="form-group">
            <input type="submit" value="Registrate" className="btn btn-success" />
          </div>
        </form>
      </div>
    </>
  )
};